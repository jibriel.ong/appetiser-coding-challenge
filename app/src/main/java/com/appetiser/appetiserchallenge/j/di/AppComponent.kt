package com.appetiser.appetiserchallenge.j.di

import android.app.Application
import com.appetiser.appetiserchallenge.j.App
import com.appetiser.appetiserchallenge.j.di.builder.ActivityBuilder
import com.appetiser.appetiserchallenge.j.di.modules.AppModule
import com.appetiser.appetiserchallenge.j.di.modules.NetworkModule
import com.appetiser.appetiserchallenge.j.di.modules.StorageModule
import com.appetiser.appetiserchallenge.j.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        StorageModule::class,
        ActivityBuilder::class,
        ViewModelModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}