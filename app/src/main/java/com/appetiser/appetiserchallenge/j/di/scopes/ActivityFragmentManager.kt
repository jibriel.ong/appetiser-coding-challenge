package com.appetiser.appetiserchallenge.j.di.scopes

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityFragmentManager