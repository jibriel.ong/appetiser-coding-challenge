package com.appetiser.appetiserchallenge.j.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.appetiser.appetiserchallenge.j.App
import dagger.android.AndroidInjection
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector

object AppInjector {
    fun init(app: App) {
        DaggerAppComponent.builder().application(app)
            .build().inject(app)
        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                handleActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) {

            }

            override fun onActivityResumed(activity: Activity) {

            }

            override fun onActivityPaused(activity: Activity) {

            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {

            }

            override fun onActivityDestroyed(activity: Activity) {

            }
        })
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector || activity is HasActivityInjector) {
            AndroidInjection.inject(activity)
        }

    }
}