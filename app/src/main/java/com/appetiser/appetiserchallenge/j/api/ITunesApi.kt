package com.appetiser.appetiserchallenge.j.api

import com.appetiser.appetiserchallenge.j.base.ItunesListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ITunesApi {

    /**
     * Http Request
     *
     * Get data from the iTunes API
     *
     * @param term
     * @param country
     * @param media
     * @param all
     *
     */

    @GET("/search")
    fun searchItunesRxjava(
        @Query("term") term: String,
        @Query("country") country: String,
        @Query("media") media: String,
        @Query("all") all: String
    ): Single<ItunesListResponse>

}