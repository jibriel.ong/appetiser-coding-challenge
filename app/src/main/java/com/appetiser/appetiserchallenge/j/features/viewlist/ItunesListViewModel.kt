package com.appetiser.appetiserchallenge.j.features.viewlist

import android.content.SharedPreferences
import android.os.Bundle
import com.appetiser.appetiserchallenge.j.base.BaseViewModel
import com.appetiser.appetiserchallenge.j.base.ItunesListResponse
import com.appetiser.appetiserchallenge.j.data.ItunesData
import com.appetiser.appetiserchallenge.j.data.repository.ITunesRepository
import com.duyp.androidutils.rx.functions.PlainConsumer
import javax.inject.Inject

class ItunesListViewModel @Inject constructor(
    iTunesRepository: ITunesRepository,
    sharedPreferences: SharedPreferences
) : BaseViewModel(iTunesRepository, sharedPreferences) {
    override fun onFirsTimeUiCreate(bundle: Bundle?) {

    }

    fun getItunesListRx(consumer: PlainConsumer<ItunesListResponse>) {
        execute(false, iTunesRepository.getItunesList(), consumer)
    }

    fun getItunesFinalList(input: ItunesListResponse?): MutableList<ItunesData> {
        return iTunesRepository.getItunes(input)
    }

    fun checkIfDbIsEmpty(): Boolean {
        return iTunesRepository.checkIfDBisEmpty()
    }

    fun updateDb(track: ItunesData, date: String) {
        iTunesRepository.updateDate(track, date)
    }

    fun getItuneTrack(position: Int): ItunesData? {
        return iTunesRepository.getSingleTrackDetails(position)

    }

    fun storePageRef(input: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt("pageNum", input)
        editor.apply()
    }


    fun getPageRef(): Int {
        return sharedPreferences.getInt("pageNum", 0)
    }

}