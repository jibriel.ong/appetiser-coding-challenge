package com.appetiser.appetiserchallenge.j.data

import androidx.lifecycle.LiveData

class SafeMutableLiveData<T> : LiveData<T>() {

    public override fun setValue(value: T) {
        try {
            super.setValue(value)
        } catch (e: Exception) {
            // if we can't set value due to not in main thread, must call post value instead
            super.postValue(value)
        }
    }
}