package com.appetiser.appetiserchallenge.j.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.appetiser.appetiserchallenge.j.data.ItunesData

@Database(entities = arrayOf(ItunesData::class), version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun itunesDao(): ItunesDao
}