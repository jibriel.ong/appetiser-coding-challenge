package com.appetiser.appetiserchallenge.j.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.appetiser.appetiserchallenge.j.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    // dispatch android injector to all fragments
    @set:Inject
    internal var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>? = null
    private var onStartCount: Int = 0
    private var progressDialog: Dialog? = null

    abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!shouldUseDataBinding()) {
            // set contentView if child activity not use dataBinding
            setContentView(layout)
        }

        if (shouldPostponeTransition()) {
            ActivityCompat.postponeEnterTransition(this)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (canBack()) {
            if (item.itemId == android.R.id.home) {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return dispatchingAndroidInjector
    }

    /**
     * @return true if should use back button on toolbar
     */
    protected abstract fun canBack(): Boolean

    /**
     * @return true if this activity should use layout stable fullscreen (status bar overlap activity's content)
     */
    protected open fun shouldUseLayoutStableFullscreen(): Boolean {
        return false
    }

    /**
     * @return true if this activity should postpone transition (in case of destination view is in viewpager)
     */
    protected open fun shouldPostponeTransition(): Boolean {
        return false
    }

    /**
     * @return true if child activity should use data binding instead of [.setContentView]
     */
    protected open fun shouldUseDataBinding(): Boolean {
        return false
    }

    protected open fun shouldOverridePendingTransition(): Boolean {
        return true
    }


    fun initProgressDialog() {
        if (progressDialog == null) {
            val view = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)
            val alertBuilder = AlertDialog.Builder(this)

            alertBuilder.setCancelable(false)
            alertBuilder.setView(view)
            alertBuilder.setCancelable(false)

            progressDialog = alertBuilder.create()
        }
    }

    fun showProgressDialog() {
        initProgressDialog()

        progressDialog?.let { dialog ->
            if (!dialog.isShowing) {
                dialog.show()
            }
        }
    }

    fun hideProgressDialog() {
        progressDialog?.let { dialog ->
            try {
                dialog.dismiss()
            } catch (ignored: Exception) {
            }
        }
    }

    open fun setLoading(loading: Boolean) {
        if (loading) {
            showProgressDialog()
        } else {
            hideProgressDialog()
        }
    }
}