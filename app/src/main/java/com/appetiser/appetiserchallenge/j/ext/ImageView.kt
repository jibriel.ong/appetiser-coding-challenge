package com.appetiser.appetiserchallenge.j.ext

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.appetiser.appetiserchallenge.j.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

@BindingAdapter("imgUrl")
fun loadImage(imageView: ImageView, url: String) {
    Glide.with(imageView.context)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .skipMemoryCache(false)
        .placeholder(R.drawable.ic_noimg_placeholder)
        .dontAnimate()
        .into(imageView)
}
