package com.appetiser.appetiserchallenge.j.features.viewdetails

import com.appetiser.appetiserchallenge.j.base.BaseActivityModule
import dagger.Module

@Module
class ViewDetailsActivityModule : BaseActivityModule<ViewDetailsActivity>()