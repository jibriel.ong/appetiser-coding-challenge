package com.appetiser.appetiserchallenge.j.data

import com.appetiser.appetiserchallenge.j.api.RestHelper
import com.duyp.androidutils.rx.functions.PlainConsumer
import io.reactivex.FlowableEmitter
import io.reactivex.Single

abstract class SimpleRemoteSourceMapper<T>(emitter: FlowableEmitter<Resource<T>>) {

    abstract val remote: Single<T>

    init {
        emitter.onNext(Resource.loading(null))
        // since realm instance was created on Main Thread, so if we need to touch on realm database after calling
        // api (such as save response data to local database, we must make request on main thread
        // by setting shouldUpdateUi params = true
        val disposable = RestHelper.makeRequest(remote, true, PlainConsumer { response ->
            saveCallResult(response)
            emitter.onNext(Resource.success(response))
        }, PlainConsumer { errorEntity ->
            emitter.onNext(Resource.error(errorEntity.message, null, errorEntity.errorCode))
        })

        // set emitter disposable to ensure that when it is going to be disposed, our api request should be disposed as well
        emitter.setDisposable(disposable)
    }

    abstract fun saveCallResult(data: T)
}