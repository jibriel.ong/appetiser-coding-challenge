package com.appetiser.appetiserchallenge.j.data

enum class Status {

    SUCCESS,
    ERROR,
    LOADING,
    PROGRESS
}