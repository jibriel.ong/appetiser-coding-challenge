package com.appetiser.appetiserchallenge.j.utils

class UploadProgressListener(val key: String) : ProgressListener {

    var progress: Double = 0.0

    private var listener: KeyProgressListener? = null

    override fun onProgress(progress: Double) {
        this.progress = progress
        listener?.onProgress(key, progress)
    }

    fun setListener(listener: KeyProgressListener) {
        this.listener = listener
    }
}