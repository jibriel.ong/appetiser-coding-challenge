package com.appetiser.appetiserchallenge.j.di.builder

import dagger.Module

@Module
abstract class FragmentBuilder