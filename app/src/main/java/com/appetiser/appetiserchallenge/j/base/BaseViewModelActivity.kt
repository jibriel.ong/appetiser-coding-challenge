package com.appetiser.appetiserchallenge.j.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.appetiser.appetiserchallenge.j.data.State
import com.appetiser.appetiserchallenge.j.data.Status
import com.appetiser.appetiserchallenge.j.utils.ViewUtils
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


abstract class BaseViewModelActivity<B : ViewDataBinding, VM : BaseViewModel> : BaseActivity() {

    protected lateinit var viewModel: VM
    @VisibleForTesting
    protected lateinit var binding: B
    @set:Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //init data binding
        binding = DataBindingUtil.setContentView(this, layout)
        binding.lifecycleOwner = this

        // int view model
        // noinspection unchecked
        val viewModelClass = (javaClass
            .genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<VM> // 1 is BaseViewModel

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClass)

        viewModel.onCreate(intent.extras)
        viewModel.stateLiveData.observe(this, Observer<State> { this.handleState(it) })
    }

    override fun shouldUseDataBinding(): Boolean {
        return true
    }

    /**
     * Default state handling, can be override
     *
     * @param state viewModel's state
     */
    protected open fun handleState(state: State?) {
        setLoading(state != null && state.status == Status.LOADING)
        handleMessageState(state)

        if (state?.status == Status.ERROR) {
            state.code?.let {

            }
        }
    }

    private fun handleMessageState(state: State?) {
        state?.message?.let { msg ->
            if (msg.isNotBlank()) {
                if (state.isHardAlert) {
                    ViewUtils.showAlertDialog(this, null, msg)
                } else {
                    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
                }
            }
        }
    }


}