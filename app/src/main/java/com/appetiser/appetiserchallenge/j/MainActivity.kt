package com.appetiser.appetiserchallenge.j

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.appetiser.appetiserchallenge.j.features.viewlist.ItunesListActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = Intent(this, ItunesListActivity::class.java)
        startActivity(intent)
        finish()
    }
}
