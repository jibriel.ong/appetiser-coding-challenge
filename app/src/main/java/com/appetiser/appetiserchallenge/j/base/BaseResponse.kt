package com.appetiser.appetiserchallenge.j.base

import com.google.gson.annotations.SerializedName

open class BaseResponse(
    @SerializedName("status")
    val status: Int? = null,
    @SerializedName("error_code")
    val errorCode: String? = null,
    @SerializedName("success")
    val success: Boolean? = null,
    @SerializedName("message")
    val message: String? = null
)