package com.appetiser.appetiserchallenge.j.base

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.appetiserchallenge.j.base.`interface`.OnItemClickListener
import com.appetiser.appetiserchallenge.j.di.scopes.ActivityContext

abstract class BaseAdapter<T>(@param:ActivityContext protected val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data: MutableList<T>? = null

    var itemClickListener: OnItemClickListener<T>? = null

    protected var layoutInflater: LayoutInflater

    init {
        setHasStableIds(true)
        this.layoutInflater = LayoutInflater.from(context)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            bindItemViewHolder(holder, item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder = createItemHolder(parent, viewType)
        holder.itemView.setOnClickListener {
            if (itemClickListener != null) {
                val item = getItem(holder.adapterPosition)
                if (item != null) {
                    itemClickListener!!.onItemClick(holder.itemView, item)
                }
            }
        }
        return holder
    }

    protected abstract fun createItemHolder(
        viewGroup: ViewGroup,
        itemType: Int
    ): RecyclerView.ViewHolder

    protected abstract fun bindItemViewHolder(viewHolder: RecyclerView.ViewHolder, data: T)

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    protected fun getItem(position: Int): T? {
        if (data != null) {
            if (position >= 0 && position < data!!.size) {
                return data!![position]
            }
        }
        return null
    }

    override fun getItemCount(): Int {
        return if (data != null) data!!.size else 0
    }

    /**
     * clear adapter data
     */
    fun clear() {
        data = null
        notifyDataSetChanged()
    }

    fun updateItem(item: T) {
        data?.let {
            val position = it.indexOf(item)
            if (position != -1) {
                changeItem(position, item)
            }
        }
    }

    fun changeItem(position: Int, item: T) {
        if (data == null || data!!.isEmpty()) {
            return
        }

        if (position >= 0 && position < data!!.size) {
            data!![position] = item
            notifyItemChanged(position)
        }
    }
}