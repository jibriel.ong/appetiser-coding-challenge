package com.appetiser.appetiserchallenge.j.api

import com.appetiser.appetiserchallenge.j.base.BaseResponse
import com.appetiser.appetiserchallenge.j.data.ErrorEntity
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.net.UnknownHostException

object ResponseHelper {

    /**
     * Get http error code from [Throwable] if it is instance of [HttpException]
     *
     * @param throwable input throwable
     * @return http code or -1 if throwable isn't a instance of [HttpException]
     */
    fun getErrorCode(throwable: Throwable): Int {
        return (throwable as? HttpException)?.code() ?: -1
    }

    /**
     * Get error response from all api's response
     *
     * @param throwable throwable instance from retrofit service's response
     * @return an instance of [BaseResponse] contains error message and some other fields
     */
    fun getErrorResponse(throwable: Throwable): BaseResponse? {
        var body: ResponseBody? = null
        if (throwable is HttpException) {
            body = throwable.response().errorBody()
        }
        if (body != null) {
            try {
                return Gson().fromJson<BaseResponse>(body.string(), BaseResponse::class.java)
            } catch (e: Exception) {

            }
        }
        return null
    }

    /**
     * Get a error message from retrofit response throwable
     *
     * @param throwable retrofit rx throwable
     * @return error message
     */
    fun getPrettifiedErrorMessage(throwable: Throwable?): String {
        return if (throwable is UnknownHostException) {
            ErrorEntity.NETWORK_UNAVAILABLE
        } else ErrorEntity.OOPS
    }
}