package com.appetiser.appetiserchallenge.j.di.builder

import com.appetiser.appetiserchallenge.j.features.viewdetails.ViewDetailsActivity
import com.appetiser.appetiserchallenge.j.features.viewdetails.ViewDetailsActivityModule
import com.appetiser.appetiserchallenge.j.features.viewlist.ItunesListActivity
import com.appetiser.appetiserchallenge.j.features.viewlist.ItunesListActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [ItunesListActivityModule::class])
    internal abstract fun ituneslistActivity(): ItunesListActivity

    @ContributesAndroidInjector(modules = [ViewDetailsActivityModule::class])
    internal abstract fun viewdetailsActivity(): ViewDetailsActivity
}