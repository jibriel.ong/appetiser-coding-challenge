package com.appetiser.appetiserchallenge.j.data

import com.appetiser.appetiserchallenge.j.api.RestHelper
import com.appetiser.appetiserchallenge.j.utils.KeyProgressListener
import com.appetiser.appetiserchallenge.j.utils.UploadProgressListener
import com.duyp.androidutils.rx.functions.PlainConsumer
import io.reactivex.FlowableEmitter
import io.reactivex.Single

abstract class ProgressRemoteSourceMapper<T>(
    emitter: FlowableEmitter<Resource<T>>,
    progressListenerMap: Map<String, UploadProgressListener>?
) {

    private val progressMap = HashMap<String, Double>()

    abstract val remote: Single<T>

    init {

        emitter.onNext(Resource.loading(null))

        if (progressListenerMap != null && !progressListenerMap.isEmpty()) {
            for ((_, value) in progressListenerMap) {
                value.setListener(object : KeyProgressListener {
                    override fun onProgress(key: String, progress: Double) {
                        progressMap[key] = progress
                        emitter.onNext(Resource.progress(null, calculateProgress()))
                    }
                })
            }
        }

        val disposable = RestHelper.makeRequest(
            remote,
            true,
            PlainConsumer { response ->
                saveCallResult(response)
                emitter.onNext(Resource.success(response))
            },
            PlainConsumer { errorEntity ->
                emitter.onNext(Resource.error(errorEntity.message, null, errorEntity.errorCode))
            })

        // set emitter disposable to ensure that when it is going to be disposed, our api request should be disposed as well
        emitter.setDisposable(disposable)
    }

    abstract fun saveCallResult(data: T)

    private fun calculateProgress(): Double {
        if (progressMap.isEmpty()) {
            return 0.0
        }

        val totalPercent = progressMap.size * 100.0
        var totalProgress = 0.0

        for (percent in progressMap.values) {
            totalProgress += percent
        }

        return totalProgress / totalPercent * 100.0
    }
}