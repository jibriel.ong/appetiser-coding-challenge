package com.appetiser.appetiserchallenge.j.features.viewlist

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.appetiserchallenge.j.R
import com.appetiser.appetiserchallenge.j.base.BaseViewModelActivity
import com.appetiser.appetiserchallenge.j.base.`interface`.OnItemClickListener
import com.appetiser.appetiserchallenge.j.data.ItunesData
import com.appetiser.appetiserchallenge.j.databinding.ActivityItunesListBinding
import com.appetiser.appetiserchallenge.j.features.viewdetails.ViewDetailsActivity
import com.appetiser.appetiserchallenge.j.utils.Helper
import com.duyp.androidutils.rx.functions.PlainConsumer
import javax.inject.Inject

class ItunesListActivity :
    BaseViewModelActivity<ActivityItunesListBinding, ItunesListViewModel>() {

    @Inject
    internal lateinit var mItemAdapter: ItunesListDataAdapter

    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override val layout: Int
        get() = R.layout.activity_itunes_list

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mHandler = Handler()
        binding.vm
        binding.swipeLayout.setOnRefreshListener {
            mRunnable = Runnable {
                getItunesListUsingInternet()
                binding.swipeLayout.isRefreshing = false
            }

            mHandler.postDelayed(
                mRunnable,
                (1000).toLong() // Delay 1 second
            )
        }
        initList()
        getItunesListUsingInternet()
    }

    private fun initList() {
        val linearLayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )

        binding.itunesRecycler.layoutManager = linearLayoutManager
        binding.itunesRecycler.adapter = mItemAdapter

        mItemAdapter.mItemClickListener = object : OnItemClickListener<ItunesData> {
            override fun onItemClick(v: View, item: ItunesData) {
                ituneItemClickListener(item)
            }
        }
    }


    private fun ituneItemClickListener(track: ItunesData) {
        var date = Helper.getDate()
        viewModel.updateDb(
            track,
            date
        )  //Stores date and time and update the corresponding item int the database
        mItemAdapter.notifyDataSetChanged()

        viewModel.storePageRef(track.counter)  //Stores screen or page ref number using SharedPreference

        Helper.trackDetails = track
        val intent = Intent(this, ViewDetailsActivity::class.java)
        startActivity(intent)
    }


    fun getItunesListUsingInternet() {
        viewModel.getItunesListRx(PlainConsumer {
            mItemAdapter.data = viewModel.getItunesFinalList(it)
            mItemAdapter.notifyDataSetChanged()
        })
    }


}