package com.appetiser.appetiserchallenge.j.features.viewdetails

import android.os.Bundle
import com.appetiser.appetiserchallenge.j.R
import com.appetiser.appetiserchallenge.j.base.BaseViewModelActivity
import com.appetiser.appetiserchallenge.j.databinding.ActivityViewDetailsBinding
import com.appetiser.appetiserchallenge.j.utils.Helper

class ViewDetailsActivity :
    BaseViewModelActivity<ActivityViewDetailsBinding, ViewDetailsViewModel>() {

    override val layout: Int
        get() = R.layout.activity_view_details

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewmodel = viewModel
        viewModel.setData(Helper.trackDetails)
    }
}