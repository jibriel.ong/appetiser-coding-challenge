package com.appetiser.appetiserchallenge.j.utils

import android.content.Context
import android.net.ConnectivityManager
import com.appetiser.appetiserchallenge.j.data.ItunesData
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

object Helper {

    // This will be used to store temporay ItunesData when displaying Movie Details (TrackDetailsActivity)

    var trackDetails = ItunesData("", "", "", "", "", "", -1, "", "")


    /**
     * Check if there's an internet connection
     *
     * @return true if there's internet connection and false if none
     */

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    /**
     * Get the current date and time
     *
     * @return date and time string
     */

    fun getDate(): String {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formatted = current.format(formatter)

        return formatted
    }
}