package com.appetiser.appetiserchallenge.j.utils

import android.text.TextUtils

object Inputs {
    fun isEmpty(text: String?): Boolean {
        return text == null || TextUtils.isEmpty(text) || isWhiteSpaces(text) || text.equals(
            "null",
            ignoreCase = true
        )
    }


    private fun isWhiteSpaces(s: String?): Boolean {
        return s != null && s.matches("\\s+".toRegex())
    }
}