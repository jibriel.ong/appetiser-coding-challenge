package com.appetiser.appetiserchallenge.j.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appetiser.appetiserchallenge.j.di.ViewModelFactory
import com.appetiser.appetiserchallenge.j.di.scopes.ViewModelKey
import com.appetiser.appetiserchallenge.j.features.viewdetails.ViewDetailsViewModel
import com.appetiser.appetiserchallenge.j.features.viewlist.ItunesListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ItunesListViewModel::class)
    internal abstract fun bindItunesViewModel(viewModel: ItunesListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ViewDetailsViewModel::class)
    internal abstract fun bindViewDetialsViewModel(viewModel: ViewDetailsViewModel): ViewModel
}