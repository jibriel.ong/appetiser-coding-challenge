package com.appetiser.appetiserchallenge.j.utils

interface KeyProgressListener {

    fun onProgress(key: String, progress: Double = 0.0)
}
