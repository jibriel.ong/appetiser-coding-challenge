package com.appetiser.appetiserchallenge.j.base

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import com.appetiser.appetiserchallenge.j.di.scopes.ActivityContext
import com.appetiser.appetiserchallenge.j.di.scopes.ActivityFragmentManager
import dagger.Module
import dagger.Provides

@Module
abstract class BaseActivityModule<T : AppCompatActivity> {

    @Provides
    @ActivityContext
    fun provideContext(activity: T): Context {
        return activity
    }

    @Provides
    fun provideActivity(activity: T): Activity {
        return activity
    }

    @Provides
    @ActivityFragmentManager
    fun provideFragmentManager(activity: T): FragmentManager {
        return activity.supportFragmentManager
    }

    @Provides
    fun provideLifeCycleOwner(activity: T): LifecycleOwner {
        return activity
    }
}