package com.appetiser.appetiserchallenge.j.features.viewdetails

import android.content.SharedPreferences
import android.os.Bundle
import com.appetiser.appetiserchallenge.j.base.BaseViewModel
import com.appetiser.appetiserchallenge.j.data.ItunesData
import com.appetiser.appetiserchallenge.j.data.repository.ITunesRepository
import javax.inject.Inject

class ViewDetailsViewModel @Inject constructor(
    iTunesRepository: ITunesRepository,
    sharedPreferences: SharedPreferences
) : BaseViewModel(iTunesRepository, sharedPreferences) {

    override fun onFirsTimeUiCreate(bundle: Bundle?) {

    }

    var itunesData: ItunesData? = null
    /**
     * Set the ItunesData to be displayed
     *
     * @param itunesData the data to be displayed
     */
    fun setData(itunesData: ItunesData) {
        this.itunesData = itunesData
    }
}