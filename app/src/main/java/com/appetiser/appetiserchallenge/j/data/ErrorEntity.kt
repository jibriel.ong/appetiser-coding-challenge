package com.appetiser.appetiserchallenge.j.data

class ErrorEntity(val message: String, val httpCode: Int, val errorCode: String? = null) {

    override fun equals(other: Any?): Boolean {
        if (this.javaClass != other!!.javaClass) {
            return false
        }
        if (this === other) {
            return true
        }
        val entity = other as ErrorEntity?
        return this.httpCode == entity!!.httpCode && this.message == entity.message
    }

    companion object {

        val HTTP_ERROR_CODE_UNAUTHORIZED = 401

        const val OOPS = "Unexpected error while requesting the server."
        const val NETWORK_UNAVAILABLE = "Network unavailable!"

        fun getError(code: Int, reason: String?): ErrorEntity {
            return reason?.let { ErrorEntity(it, code) } ?: ErrorEntity(OOPS, code)
        }

        val errorOops: ErrorEntity
            get() = ErrorEntity(OOPS, 0)
    }
}