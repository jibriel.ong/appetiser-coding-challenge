package com.appetiser.appetiserchallenge.j.api

import com.appetiser.appetiserchallenge.j.data.ErrorEntity
import com.appetiser.appetiserchallenge.j.data.ProgressRemoteSourceMapper
import com.appetiser.appetiserchallenge.j.data.Resource
import com.appetiser.appetiserchallenge.j.data.SimpleRemoteSourceMapper
import com.appetiser.appetiserchallenge.j.utils.UploadProgressListener
import com.duyp.androidutils.rx.functions.PlainConsumer
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

object RestHelper {

    /**
     * Create new retrofit api request
     *
     * @param request          single api request
     * @param shouldUpdateUi   true if should update UI after request done
     * @param responseConsumer consume parsed response data (in pojo object)
     * @param errorConsumer    consume [ErrorEntity] object which is construct by which error is occurred
     * with a code and an message (will be shown to user)
     * the error might by a HttpException, Runtime Exception, or an error respond from back-end api...
     * @param <T>              Type of response body
     * @return a disposable
    </T> */
    fun <T> makeRequest(
        request: Single<T>, shouldUpdateUi: Boolean,
        responseConsumer: PlainConsumer<T>,
        errorConsumer: PlainConsumer<ErrorEntity>?
    ): Disposable {

        var single = request.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.io())
        if (shouldUpdateUi) {
            single = single.observeOn(AndroidSchedulers.mainThread())
        }

        return single.subscribe(responseConsumer, PlainConsumer { throwable ->
            // handle error
            if (errorConsumer != null) {
                val httpCode = ResponseHelper.getErrorCode(throwable)
                val errorResponse = ResponseHelper.getErrorResponse(throwable)
                val message =
                    errorResponse?.message ?: ResponseHelper.getPrettifiedErrorMessage(throwable)

                errorConsumer.accept(ErrorEntity(message, httpCode, errorResponse?.errorCode))
            }
        })
    }

    /**
     * Create a mapper from retrofit service to [Resource] with rx's [Flowable]
     * To indicate current state while execute an rest api (loading, error, success with status and message if error)
     *
     * @param remote from retrofit service
     * @param onSave will be called after success response come, to save response data into local database
     * @param <T>    type of response
     * @return a [Flowable] instance to deal with progress showing and error handling
    </T> */
    fun <T> createRemoteSourceMapper(
        remote: Single<T>,
        onSave: PlainConsumer<T>
    ): Flowable<Resource<T>> {
        return Flowable.create({ emitter ->
            object : SimpleRemoteSourceMapper<T>(emitter) {
                override val remote: Single<T>
                    get() = remote

                override fun saveCallResult(data: T) {
                    onSave.accept(data)
                }
            }
        }, BackpressureStrategy.BUFFER)
    }

    fun <T> createRemoteSourceProgressMapper(
        remote: Single<T>,
        onSave: PlainConsumer<T>,
        progressListenerMap: Map<String, UploadProgressListener>?
    ): Flowable<Resource<T>> {
        return Flowable.create({ emitter ->
            object : ProgressRemoteSourceMapper<T>(emitter, progressListenerMap) {
                override val remote: Single<T>
                    get() = remote

                override fun saveCallResult(data: T) {
                    onSave.accept(data)
                }
            }
        }, BackpressureStrategy.BUFFER)
    }
}