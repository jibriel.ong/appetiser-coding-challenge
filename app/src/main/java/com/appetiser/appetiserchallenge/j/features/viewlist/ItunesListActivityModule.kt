package com.appetiser.appetiserchallenge.j.features.viewlist

import com.appetiser.appetiserchallenge.j.base.BaseActivityModule
import dagger.Module

@Module
class ItunesListActivityModule : BaseActivityModule<ItunesListActivity>()