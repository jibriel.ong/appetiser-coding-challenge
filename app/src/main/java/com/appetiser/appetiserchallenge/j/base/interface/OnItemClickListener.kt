package com.appetiser.appetiserchallenge.j.base.`interface`

import android.view.View

interface OnItemClickListener<T> {
    fun onItemClick(v: View, item: T)
}