package com.appetiser.appetiserchallenge.j.features.viewlist

import android.content.Context
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.appetiserchallenge.j.R
import com.appetiser.appetiserchallenge.j.base.BaseAdapter
import com.appetiser.appetiserchallenge.j.base.BaseViewHolder
import com.appetiser.appetiserchallenge.j.base.`interface`.OnItemClickListener
import com.appetiser.appetiserchallenge.j.data.ItunesData
import com.appetiser.appetiserchallenge.j.databinding.ListTunesItemBinding
import com.appetiser.appetiserchallenge.j.di.scopes.ActivityContext
import kotlinx.android.synthetic.main.list_tunes_item.view.*
import javax.inject.Inject

class ItunesListDataAdapter @Inject constructor(
    @ActivityContext context: Context
) : BaseAdapter<ItunesData>(context) {

    var mItemClickListener: OnItemClickListener<ItunesData>? = null


    internal inner class ItemViewHolder(private val binding: ListTunesItemBinding) :
        BaseViewHolder<ItunesData>(binding.root) {

        override fun bind(item: ItunesData) {

            itemView.itunesCard.setOnClickListener {
                if (mItemClickListener != null) {
                    if (item != null) {
                        mItemClickListener!!.onItemClick(binding.itunesCard, item)
                    }
                }
            }
            this.binding.item = item
            this.binding.executePendingBindings()
        }
    }

    override fun createItemHolder(viewGroup: ViewGroup, itemType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ListTunesItemBinding>(
            layoutInflater,
            R.layout.list_tunes_item,
            viewGroup,
            false
        )
        val holder = ItemViewHolder(binding)

        return holder
    }


    override fun bindItemViewHolder(viewHolder: RecyclerView.ViewHolder, data: ItunesData) {
        (viewHolder as ItemViewHolder).bind(data)
    }

}