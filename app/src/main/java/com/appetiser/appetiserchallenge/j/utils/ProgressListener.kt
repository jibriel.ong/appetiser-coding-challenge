package com.appetiser.appetiserchallenge.j.utils

interface ProgressListener {

    fun onProgress(progress: Double = 0.0)
}