package com.appetiser.appetiserchallenge.j.utils

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat

object ViewUtils {
    fun showAlertDialog(context: Context, title: String?, body: String) {
        showAlertDialog(context, title, body) { dialog: DialogInterface, _: Int ->
            dialog.dismiss()
        }
    }

    fun showAlertDialog(
        context: Context, title: String?, body: String,
        dialogInterface: (DialogInterface, Int) -> Unit
    ) {

        if (body.isEmpty()) {
            return
        }

        val alertBuilder = AlertDialog.Builder(context)
        alertBuilder.setTitle(title)
        alertBuilder.setMessage(HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_LEGACY))
        alertBuilder.setPositiveButton(context.getString(android.R.string.ok), dialogInterface)
        alertBuilder.create().show()
    }

}