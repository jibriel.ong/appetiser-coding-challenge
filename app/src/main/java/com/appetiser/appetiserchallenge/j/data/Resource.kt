package com.appetiser.appetiserchallenge.j.data

class Resource<T> {

    val state: State

    val data: T?

    constructor(status: Status, data: T?, message: String?, code: String? = null) {
        this.state = State(status, message, null, code)
        this.data = data
    }

    constructor(status: Status, data: T?, message: String?, progress: Double?) {
        this.state = State(status, message, progress, null)
        this.data = data
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val resource = other as Resource<*>?

        return if (resource!!.state == this.state && data != null) data == resource.data else resource.data == null
    }

    override fun hashCode(): Int {
        var result = state.hashCode()
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Resource{" +
                "status=" + state.status +
                ", progress=" + state.progress +
                ", message='" + state.message + '\''.toString() +
                ", data=" + data +
                '}'.toString()
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?, errorCode: String?): Resource<T> {
            return Resource(Status.ERROR, data, msg, errorCode)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> progress(data: T?, progress: Double): Resource<T> {
            return Resource(Status.PROGRESS, data, null, progress)
        }
    }
}