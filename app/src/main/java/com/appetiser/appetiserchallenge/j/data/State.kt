package com.appetiser.appetiserchallenge.j.data

class State(
    val status: Status,
    val message: String? = null,
    var progress: Double? = null,
    var code: String? = null
) {
    var isHardAlert = false

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val state = other as State?

        if (status != state!!.status) {
            return false
        }

        return if (message != null) message == state.message else state.message == null
    }

    override fun hashCode(): Int {
        val result = status.hashCode()
        return 31 * result + if (message != null) message.hashCode() else 0
    }

    override fun toString(): String {
        return "status: $status, message: $message"
    }

    companion object {

        fun loading(): State {
            return State(Status.LOADING, null)
        }

        fun loading(message: String): State {
            return State(Status.LOADING, message)
        }

        fun progress(progress: Double): State {
            return State(Status.PROGRESS, null, progress)
        }

        fun progress(message: String, progress: Double): State {
            return State(Status.PROGRESS, message, progress)
        }

        fun error(message: String): State {
            return State(Status.ERROR, message)
        }

        fun error(message: String, code: String): State {
            return State(Status.ERROR, message, null, code)
        }

        fun success(): State {
            return State(Status.SUCCESS, null)
        }

        fun success(message: String): State {
            return State(Status.SUCCESS, message)
        }
    }
}