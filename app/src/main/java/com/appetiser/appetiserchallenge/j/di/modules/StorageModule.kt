package com.appetiser.appetiserchallenge.j.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.appetiser.appetiserchallenge.j.data.local.AppDataBase
import com.appetiser.appetiserchallenge.j.data.local.ItunesDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun provideSharedPreference(app: Application): SharedPreferences =
        app.getSharedPreferences("shared pref", Context.MODE_PRIVATE)


    @Provides
    @Singleton
    fun providesAppDatabase(app: Application): AppDataBase =
        Room.databaseBuilder(
            app.applicationContext,
            AppDataBase::class.java,
            "itunes-db"
        ).allowMainThreadQueries().build()

    @Provides
    @Singleton
    fun providesToDoDao(database: AppDataBase): ItunesDao {
        return database.itunesDao()
    }
}