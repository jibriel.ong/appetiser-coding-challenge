package com.appetiser.appetiserchallenge.j.base

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.appetiser.appetiserchallenge.j.data.Resource
import com.appetiser.appetiserchallenge.j.data.SafeMutableLiveData
import com.appetiser.appetiserchallenge.j.data.State
import com.appetiser.appetiserchallenge.j.data.Status
import com.appetiser.appetiserchallenge.j.data.repository.ITunesRepository
import com.appetiser.appetiserchallenge.j.utils.Inputs
import com.duyp.androidutils.rx.functions.PlainConsumer
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel(
    protected val iTunesRepository: ITunesRepository,
    protected val sharedPreferences: SharedPreferences
) : ViewModel() {

    val stateLiveData = SafeMutableLiveData<State>()
    private var isFirstTimeUiCreate = true
    private var compositeDisposable = CompositeDisposable()

    /**
     * called after fragment / activity is created with input bundle arguments
     *
     * @param bundle argument data
     */
    @CallSuper
    fun onCreate(bundle: Bundle?) {
        if (isFirstTimeUiCreate) {
            onFirsTimeUiCreate(bundle)
            isFirstTimeUiCreate = false
        }
    }

    /**
     * Called when UI create for first time only, since activity / fragment might be rotated,
     * we don't need to re-init data, because view model will survive, data aren't destroyed
     *
     * @param bundle
     */
    protected abstract fun onFirsTimeUiCreate(bundle: Bundle?)

    fun disposeAllExecutions() {
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
        publishState(State.success())
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun addDisposables(vararg disposables: Disposable) {
        compositeDisposable.addAll(*disposables)
    }

    /**
     * Add and execute an resource flowable created by
     * [RestHelper.createRemoteSourceMapper]
     * Loading, error, success status will be updated automatically via [.stateLiveData] which should be observed
     * by fragments / activities to update UI appropriately
     *
     * @param showProgress     true if should show progress when executing, false if not
     * @param resourceFlowable flowable resource, see [SimpleRemoteSourceMapper]
     * @param responseConsumer consume response data
     * @param <T>              type of response
    </T> */
    protected fun <T> execute(
        showProgress: Boolean, resourceFlowable: Flowable<Resource<T>>,
        responseConsumer: PlainConsumer<T>?
    ) {

        val disposable = resourceFlowable.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe { resource ->
                if (resource != null) {
                    if (resource.data != null && responseConsumer != null) {
                        responseConsumer.accept(resource.data)
                    }
                    if (resource.state.status == Status.LOADING && !showProgress) {
                        // do nothing if progress showing is not allowed
                    } else {

                        publishState(resource.state)
                    }
                }
            }
        compositeDisposable.add(disposable)
    }

    fun publishState(state: State) {
        stateLiveData.setValue(state)
        if (!Inputs.isEmpty(state.message)) {
            // if state has a message, after show it, we should reset to prevent
            // message will still be shown if fragment / activity is rotated (re-observe state live data)
            Handler().postDelayed({ stateLiveData.setValue(State.success()) }, 100)
        }
    }


}