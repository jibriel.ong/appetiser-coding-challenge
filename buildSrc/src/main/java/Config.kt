const val kotlinVersion = "1.3.21"
const val androidPluginVersion = "3.2.1"
const val dexCountVersion = "0.8.2"
const val googlePlayVersion = "4.2.0"
const val fabricPluginVersion = "1.25.4"

// Compile dependencies
const val appCompatVersion = "1.1.0-alpha05"
const val cardViewVersion = "1.0.0"
const val constraintLayoutVersion = "2.0.0-beta1"
const val archCompVersion = "2.2.0-alpha01"
const val archExtensionVersion = archCompVersion
const val timberVersion = "4.7.1"
const val retrofitVersion = "2.5.0"
const val retrofitScalarVersion = "2.5.0"
const val okhttpVersion = "3.14.1"
const val okhttpLoggingVersion = "3.14.0"
const val rxJava2Version = "2.1.6"
const val rxKotlinVersion = "2.1.0"
const val rxAndroidVersion = "2.0.1"
const val rxBindingVersion = "2.0.0"
const val rxMathVersion = "0.20.0"
const val daggerVersion = "2.21"
const val multiDexVersion = "2.0.1"
const val glideVersion = "4.9.0"
const val glassFishVersion = "10.0-b28"
const val supportMaterialDesignVersion = "1.1.0-alpha06"
const val epoxyVersion = "2.19.0"
const val circleImageViewVersion = "2.2.0"
const val snapHelperVersion = "1.5"
const val runtimePermissionVersion = "0.10.2"
const val threeTenABPVersion = "1.1.1"
const val facebookSdkVersion = "4.41.0"
const val stethoVersion = "1.5.1"
const val stethoInterceptorVersion = "1.5.0"
const val rxUtilsVersion = "2.2.8"
const val crashlyticsVersion = "2.10.1@aar"
const val firebaseVersion = "16.0.1"
const val ultraViewPagerVersion = "1.0.7.7@aar"
const val dateTimePickerVersion = "ed51a3eecf"
const val rangeSeekBarVersion = "1.1.3"
const val phoneFieldVersion = "0.1.3@aar"
const val pixImagePickerVersion = "dcc34ef0a7"
const val flowLayoutVersion = "1.2.4"
const val supportCustomTabsVersion = "28.0.0"
const val securePreferencesVersion = "0.1.7"
const val fbAccountKitSdkVersion = "4.39.0"
const val playServicesVersion = "16.0.1"
const val playServicesPhoneVersion = "16.0.0"
const val playServicesLocationVersion = "16.0.0"
const val roomVersion = "2.1.0-alpha06"
const val materialDayPickerVersion = "0.4.0"
const val weekdaysSelectorVersion = "1.1.0"
const val editTagVersion = "v1.2.4-beta2"
const val mapBoxPlacesVersion = "0.8.0"
const val reactiveLocationVersion = "2.1@aar"
const val cardStackViewVersion = "2.3.4"
const val yoyoEasingVersion = "2.0@aar"
const val yoyoAnimationsVersion = "2.3@aar"
const val firebaseMessagingVersion = "18.0.0"
const val photoViewVersion = "2.3.0"
const val gravitySnapHelperVersion = "2.0"

// Wasabeef glide transformation
const val glideTransformationVersion = "4.0.1"

const val viewPagerVersion = "1.0.0"

// Test dependency versions
const val junitVersion = "4.12"
const val testRunnerVersion = "1.1.0"
const val espressoVersion = "3.1.0"

// https://proandroiddev.com/gradle-dependency-management-with-kotlin-94eed4df9a28

object BuildPlugins {
    val androidPlugin = "com.android.tools.build:gradle:$androidPluginVersion"
    val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    val dexCountPlugin = "com.getkeepsafe.dexcount:dexcount-gradle-plugin:$dexCountVersion"
    val googlePlayPlugin = "com.google.gms:google-services:$googlePlayVersion"
    val fabricPlugin = "io.fabric.tools:gradle:$fabricPluginVersion"
    val kotlinLibVesion = kotlinVersion
}

object Android {
    // Manifest version information!
    private const val versionMajor = 0
    private const val versionMinor = 0
    private const val versionPatch = 9
    private const val versionBuild = 0 // bump for dogfood builds, public betas, etc.

    const val versionCode =
            versionMajor * 10000 + versionMinor * 1000 + versionPatch * 100 + versionBuild
    const val versionName = "$versionMajor.$versionMinor.$versionPatch"

    const val compileSdkVersion = 28
    const val targetSdkVersion = 28
    const val minSdkVersion = 21
}

object Libs {

    val kotlinStdlb = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    val appCompat = "androidx.appcompat:appcompat:$appCompatVersion"
    val cardView = "androidx.cardview:cardview:$cardViewVersion"
    val constraintLayout =
            "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
    val archExtensions = "androidx.lifecycle:lifecycle-extensions:$archExtensionVersion"
    val archExtensionsCompiler = "androidx.lifecycle:lifecycle-compiler:$archExtensionVersion"
    val room = "androidx.room:room-runtime:$roomVersion"
    val roomRx = "androidx.room:room-rxjava2:$roomVersion"
    val roomCompiler = "androidx.room:room-compiler:$roomVersion"
    val archCoreTesting = "android.arch.core:core-testing:$archCompVersion"
    val roomTesting = "androidx.room:room-testing:$roomVersion"
    val timber = "com.jakewharton.timber:timber:$timberVersion"
    val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
    val retrofitRxJava2 = "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"
    val retrofitGson = "com.squareup.retrofit2:converter-gson:$retrofitVersion"
    val retrofitScalar = "com.squareup.retrofit2:converter-scalars:$retrofitScalarVersion"
    val okhttp = "com.squareup.okhttp3:okhttp:$okhttpVersion"
    val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:$okhttpLoggingVersion"
    val rxjava2 = "io.reactivex.rxjava2:rxjava:$rxJava2Version"
    val rxkotlin = "io.reactivex.rxjava2:rxkotlin:$rxKotlinVersion"
    val rxandroid = "io.reactivex.rxjava2:rxandroid:$rxAndroidVersion"
    val rxmath = "com.github.akarnokd:rxjava2-extensions:$rxMathVersion"
    val rxbindings = "com.jakewharton.rxbinding2:rxbinding-kotlin:$rxBindingVersion"
    val dagger = "com.google.dagger:dagger:$daggerVersion"
    val daggerCompiler = "com.google.dagger:dagger-compiler:$daggerVersion"
    val daggerAndroid = "com.google.dagger:dagger-android:$daggerVersion"
    val daggerSupport = "com.google.dagger:dagger-android-support:$daggerVersion"
    val daggerProcessor = "com.google.dagger:dagger-android-processor:$daggerVersion"
    val multiDex = "androidx.multidex:multidex:$multiDexVersion"
    val glide = "com.github.bumptech.glide:glide:$glideVersion"
    val glideCompiler = "com.github.bumptech.glide:compiler:$glideVersion"
    val glassFishAnnotation = "org.glassfish:javax.annotation:$glassFishVersion"
    val supportDesign = "com.google.android.material:material:$supportMaterialDesignVersion"
    val epoxy = "com.airbnb.android:epoxy:$epoxyVersion"
    val epoxyProcessor = "com.airbnb.android:epoxy-processor:$epoxyVersion"
    val circleImageView = "de.hdodenhof:circleimageview:$circleImageViewVersion"
    val snapHelperPlugin = "com.github.rubensousa:gravitysnaphelper:$snapHelperVersion"
    val runtimePermission = "com.github.tbruyelle:rxpermissions:$runtimePermissionVersion"
    val threeTenABP = "com.jakewharton.threetenabp:threetenabp:$threeTenABPVersion"
    val stetho = "com.facebook.stetho:stetho:$stethoVersion"
    val stethoInterceptor = "com.facebook.stetho:stetho-okhttp3:$stethoInterceptorVersion"
    val rxUtils = "com.github.duyp.android-utils:rx:$rxUtilsVersion"
    val crashlytics = "com.crashlytics.sdk.android:crashlytics:$crashlyticsVersion"
    val firebaseCore = "com.google.firebase:firebase-core:$firebaseVersion"
    val ultraViewPager = "com.alibaba.android:ultraviewpager:$ultraViewPagerVersion"
    val dateTimePicker = "com.github.matangrolly:SingleDateAndTimePicker:$dateTimePickerVersion"
    val rangeSeekBar = "com.crystal:crystalrangeseekbar:$rangeSeekBarVersion"
    val phoneField = "com.lamudi.phonefield:phone-field:$phoneFieldVersion"
    val pixImagePicker = "com.github.matangrolly:PixImagePicker:$pixImagePickerVersion"
    val flowLayout = "com.nex3z:flow-layout:$flowLayoutVersion"
    val supportCustomTabs = "com.android.support:customtabs:$supportCustomTabsVersion"
    val facebookSdk = "com.facebook.android:facebook-login:$facebookSdkVersion"
    val securePreferences = "com.scottyab:secure-preferences-lib:$securePreferencesVersion"
    val weekdaySelector = "com.github.DavidProdinger:weekdays-selector:$weekdaysSelectorVersion"
    val editTag = "com.github.qiugang:EditTag:$editTagVersion"
    val fbAccountKitSdk = "com.facebook.android:account-kit-sdk:$fbAccountKitSdkVersion"
    val playServicesBase = "com.google.android.gms:play-services-base:$playServicesVersion"
    val playServicesAuth = "com.google.android.gms:play-services-auth:$playServicesVersion"
    val playServicesStats = "com.google.android.gms:play-services-stats:$playServicesVersion"
    val playServicesPhone = "com.google.android.gms:play-services-auth-api-phone:$playServicesPhoneVersion"
    val playServicesLocation = "com.google.android.gms:play-services-location:$playServicesLocationVersion"
    val playServicesPlaces = "com.google.android.gms:play-services-places:$playServicesLocationVersion"
    val mapBoxPlaces = "com.mapbox.mapboxsdk:mapbox-android-plugin-places-v7:$mapBoxPlacesVersion"
    val firebaseDynamicLinks = "com.google.firebase:firebase-dynamic-links:$firebaseVersion"
    val reactiveLocation = "pl.charmas.android:android-reactive-location2:$reactiveLocationVersion"
    val cardStackView = "com.yuyakaido.android:card-stack-view:$cardStackViewVersion"
    val yoyoEasing = "com.daimajia.easing:library:$yoyoEasingVersion"
    val yoyoAnimations = "com.daimajia.androidanimations:library:$yoyoAnimationsVersion"
    val firebaseMessaging = "com.google.firebase:firebase-messaging:$firebaseMessagingVersion"
    val photoView = "com.github.chrisbanes:PhotoView:$photoViewVersion"
    val gravitySnapHelper = "com.github.rubensousa:gravitysnaphelper:$gravitySnapHelperVersion"
    // Wasabeef glide transformation
    val glideTransformation = "jp.wasabeef:glide-transformations:$glideTransformationVersion"
    val viewPager = "androidx.viewpager:viewpager:$viewPagerVersion"
}

object TestLibs {
    val junit = "junit:junit:$junitVersion"
    val testRunner = "androidx.test:runner:$testRunnerVersion"
    val espresso = "androidx.test.espresso:espresso-core:$espressoVersion"
}

